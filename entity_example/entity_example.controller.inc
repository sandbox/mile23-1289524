<?php

/**
 * @file
 * Controller class definition file for the example "DUG meeting topic" entity.
 */

/**
 * DUG Meeting Topic Controller
 */
class DugMeetingTopicController extends GenericCRUDEntityController {
/**
 * Create function.
 *
 * Overrides superclass to give us some nice initialization.
 */
  public function create_entity() {
    $topic = parent::create_entity();
    $topic->date = format_date(REQUEST_TIME, 'custom', 'Y-m-d H:i:s');
    $topic->topic = '';
    $topic->speaker = '';
    return $topic;
  }
}
