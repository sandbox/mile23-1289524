<?php

/**
 * @file
 * Page callbacks include file for the example "DUG meeting topic" entity.
 */

/**
 * DUG meeting topic list page callback.
 */
function entity_example_list_page() {
  $elements = array();

  // entity_load() called with one argument loads all entities of the specified type.
  $topics = entity_load('dug_meeting_topic');

  $header = array(t('Topic'), t('Speaker'), t('Date'));

  if(user_access('administer dug meeting topics')) {
    $header[] = t('Administer');
  }

  // Sort the topics by date, since entity_load() returns them unsorted.
  uasort($topics, create_function('$a,$b', 'return strcmp($a->date, $b->date);'));

  $rows = array();
  foreach($topics as $tid => $topic) {
    $row = array();
    $row[] = check_plain($topic->topic);
    $row[] = check_plain($topic->speaker);
    $row[] = $topic->date;

    if(user_access('administer dug meeting topics')) {
      $act = l(t('edit'), "meeting-topics/$tid/edit", array('query' => drupal_get_destination()));
      $act .= ', ';
      $act .= l(t('delete'), "meeting-topics/$tid/delete", array('query' => drupal_get_destination()));
      $row[] = $act;
    }

    $rows[] = $row;
  }
  $elements['topics'] = array(
    '#theme' => 'table',
    '#title' => t('DUG meeting topics'),
    '#header' => $header,
    '#rows' => $rows,
  );
  return $elements;
}

/**
 * DUG meeting topic add page callback.
 */
function entity_example_add() {
  // Invoke the create() method we added to the controller to get a new entity with defaults.
  $topic = entity_get_controller('dug_meeting_topic')->create_entity();

  return drupal_get_form('entity_example_form', $topic);
}

/**
 * DUG meeting topic add/edit form callback.
 */
function entity_example_form($form, &$form_state, $topic) {
  $form['#topic'] = $topic;

  $date = date_parse($topic->date);
  $form['date'] = array(
    '#type' => 'date',
    '#title' => t('Meeting date'),
    '#default_value' => $date,
    '#required' => TRUE,
  );
  $form['topic'] = array(
    '#type' => 'textfield',
    '#title' => t('Topic'),
    '#default_value' => check_plain($topic->topic),
    '#size' => 50,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['speaker'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of speaker'),
    '#default_value' => check_plain($topic->speaker),
    '#size' => 50,
    '#maxlength' => 255,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if(!empty($topic->tid)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#access' => user_access('administer dug meeting topics'),
      '#submit' => array('entity_example_form_delete_submit'),
    );
  }

  return $form;
}

/**
 * DUG meeting topic add/edit form submit callback.
 */
function entity_example_form_submit($form, &$form_state) {
  $topic = $form['#topic'];

  $date = $form_state['values']['date'];

  $topic->date = sprintf('%4d-%02d-%02d', $date['year'], $date['month'], $date['day']);
  $topic->topic = $form_state['values']['topic'];
  $topic->speaker = $form_state['values']['speaker'];

  // Invoke the save() method that we added to the controller.
  entity_get_controller('dug_meeting_topic')->save_entity($topic);
  drupal_set_message(t('Meeting topic saved'));
  $form_state['redirect'] = 'meeting-topics';
}

/**
 * DUG meeting topic delete submit callback.
 *
 * The delete form logic was adapted from
 * Drupal 7 Module Development, by Butcher, et al. (Packt, 2010)
 */
function entity_example_form_delete_submit($form, &$form_state) {
  $destination = array();
  if(isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $topic = $form['#topic'];
  $form_state['redirect'] = array(
    'meeting-topics/' . $topic->tid . '/delete',
    array('query' => $destination),
  );
}

/**
 * DUG meeting topic delete confirm form callback.
 */
function entity_example_delete_confirm($form, &$form_state, $topic) {
  $form['#topic'] = $topic;
  $form['tid'] = array(
    '#type' => 'value',
    '#value' => $topic->tid,
  );
  return confirm_form($form,
    t('Are you sure you want to delete @topic?', array('@topic' => $topic->topic)),
    '<front>',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * DUG meeting topic delete confirm form submit callback.
 */
function entity_example_delete_confirm_submit($form, &$form_state) {
  if($form_state['values']['confirm']) {
    $topic = entity_example_load($form_state['values']['tid']);

    // Invoke the delete() method that we added to the controller.
    entity_get_controller('dug_meeting_topic')->delete_entity($topic);
    drupal_set_message(t('Topic "@topic" has been deleted.', array('@topic' => $topic->topic)));
  }
  if(isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  else {
    $destination = array('destination' => '<front>');
  }
  $form_state['redirect'] = $destination['destination'];
}
