<?php

/**
 * @file
 * Generic CRUD entity controller.
 *
 * This class exists as a template, but you can also use it by subclassing.
 *
 * GenericCRUDEntityController is a subclass of DrupalDefaultEntityController.
 * You can use it as-is to add CRUD (CReate, Update, Delete) capabilities
 * to your entity implementation, or you can subclass it for your own needs.
 *
 * It provides a method called invoke_hook() which you can override to invoke
 * your own API of hooks.
 *
 * DrupalDefaultEntityController provides a load() function. This class
 * provides:
 *  * create_entity()
 *  * save_entity()
 *  * save_many_entities()
 *  * delete_many_by_id()
 *  * delete_entity()
 *
 * It attempts to handle versioning, but this is somewhat untested. In fact,
 * anyone wishing to contribute to this project could write tests. :-)
 *
 * GenericCRUDEntityController draws heavily on the core node module and
 * the entity CRUD tests for inspiration.
 *
 * Though I can only guess, since I'm too lazy to research it, the reason
 * none of this is in core Drupal 7 is because it can't ever be complete
 * for all circumstances.
 *
 * However, I was somewhat shocked to discover that the Drupal 7 entity
 * system does not call its own CRUD hooks, only some of them. That is,
 * if you implement your own entity through DrupalDefaultEntityController,
 * you have to call some of these hooks yourself, leading to a lot of
 * confusion about which ones, and how and when they should be called.
 *
 * This makes the API confusing at best.
 *
 * So I thought I'd: a) Attempt to understand this by writing my own subclass
 * and b) Make a reference project for other developers.
 */

/**
 * GenericCRUDEntityController, adds some CRUD to DrupalDefaultEntityController
 *
 * Turns DrupalDefaultEntityController into a factory as well as a
 * controller. Member functions let you create, save, and delete
 * entities. All entity hooks are called.
 */
class GenericCRUDEntityController extends DrupalDefaultEntityController {
/**
 * Create an entity.
 *
 * Call parent::create_entity() if you override this function.
 */
  public function create_entity($args = array()) {
    $entity = (object) $args;
    $entity->is_new = TRUE;
    return $entity;
  }

/**
 * Save many entities to the database.
 *
 * Convenience function.
 */
  public function save_many_entities($entities = array()) {
    foreach ($entities as $entity) {
      $this->save($entity);
    }
  }

/**
 * save_entity() function
 *
 * Adapted from node_save().
 */
  public function save_entity(&$entity) {
    // Store entity id key and entity type locally for code clarity.
    $id = $this->idKey;
    $entityType = $this->entityType;
    // Initialize revision info as empty.
    $revisionTable = '';
    $revisionID = '';

    // Add the revision info if it exists.
    if (array_key_exists('revision table', $this->entityInfo)) {
      $revisionTable = $this->entityInfo['revision table'];
    }
    if (array_key_exists('revision', $this->entityInfo['entity keys'])) {
      $revisionID = $this->entityInfo['entity keys']['revision'];
    }
    // if there's no entity id, then this entity is probably new.
    if (!isset($entity->is_new)) {
      $entity->is_new = empty($entity->$id);
    }
    if ($revisionTable != '') {
      // if the entity is not new and has a revision, then
      // we'll be doing an insert of a new revision, so
      // remove the id.
      if (!$entity->is_new && !empty($entity->revision)) {
        unset($entity->$id);
      }
    }

    // Let other modules have a shot at the fields before we save them.
    // field_attach_presave() only calls field-related hooks and doesn't
    // do any attaching or saving.
    field_attach_presave($entityType, $entity);
    // Call hook_entity_presave(). Core Entity API puts this responsibility
    // on us.
    module_invoke_all('entity_presave', $entity, $entityType);

    $update_entity = TRUE;
    if ($entity->is_new) {
      drupal_write_record($entityType, $entity);
      if ($revisionTable != '') drupal_write_record($revisionTable, $entity);
      $op = 'insert';
    }
    else {
      drupal_write_record($entityType, $entity, $id);
      if ($revisionTable != '') {
        if (!empty($entity->revision)) {
          drupal_write_record($revisionTable, $entity);
        }
        else {
          drupal_write_record($revisionTable, $entity, $revisionID);
          $update_entity = FALSE;
        }
      }
      $op = 'update';
    }
    // Mark the entity for the new revision, if there is one.
    if (($update_entity) & ($revisionID != '')) {
      db_update($this->entityType)
        ->fields(array($revisionID => $entity->$revisionID))
        ->condition($id, $entity->$id)
        ->execute();
    }

    // Call the hooks that all the other modules are expecting.
    // TODO: Determine if this should happen after fields are saved.
    $this->invoke_hook($op, $entity);
    module_invoke_all('entity_' . $op, $entity, $entityType);
  
    // Save fields if this entity is fieldable.
    if ((array_key_exists('fieldable', $this->entityInfo)) &
      ($this->entityInfo['fieldable'])) {
      $function = "field_attach_$op";
      $function($entityType, $entity);
    }
    return $entity;
  }

/**
 * Delete many entities by ID.
 *
 * Adapted from node_delete_multiple().
 */
  public function delete_many_by_id($ids) {
    $transaction = db_transaction();
    if (!empty($ids)) {
      $entities = $this->load($ids);
  
      try {
        foreach ($entities as ${$this->idKey} => $entity) {
          // Call the controller-specific callback (if any):
          $this->invoke_hook($entity, 'delete');
          // Call hook_entity_delete().
          module_invoke_all('entity_delete', $entity, $this->entityType);
          // Delete attached fields
          field_attach_delete($this->entityType, $entity);
  
          // Remove this node from the search index if needed.
          if (module_exists('search')) {
            search_reindex(${$this->idKey}, $this->entityType);
          }
        }

        // Now that we've called the hooks, we can do the deletion.
        // Delete the revisions if there are any.
        if (array_key_exists('revision table', $this->entityInfo)) {
          db_delete($this->entityInfo['revision table'])
            ->condition($this->idKey, $ids, 'IN')
            ->execute();
        }
        // Delete the entities.
        db_delete($this->entityType)
          ->condition($this->idKey, $ids, 'IN')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception($this->entityType, $e);
        throw $e;
      }
  
      $this->resetCache();
    }
  }

/**
 * Delete an entity
 */
  public function delete_entity(stdClass $entity) {
    $id = $this->idKey;
    $this->delete_many_by_id(array($entity->$id));
  }

/**
 * Extra hooks you could support.
 *
 * You should override this in your subclass in order to create your own
 * API of hooks.
 */
  public function invoke_hook($hook, $entity) {
    // This method is a no-op. Override it in your own subclass.
    // A generic implementation might look like like this:
    //   module_invoke_all($this->entityType . '_' . $hook,
    //     $entity, $this->entityType);
  }
}
